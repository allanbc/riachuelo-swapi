package br.com.riachuelo.api.starwars;

import static org.junit.Assert.assertEquals;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import br.com.riachuelo.api.starwars.controllers.dto.FilmFilter;
import br.com.riachuelo.api.starwars.controllers.dto.FilmRequest;
import br.com.riachuelo.api.starwars.controllers.dto.FilmResponse;
import br.com.riachuelo.api.starwars.models.Film;
import br.com.riachuelo.api.starwars.repository.FilmRepository;
import br.com.riachuelo.api.starwars.repository.specification.FilmSpecification;
import br.com.riachuelo.api.starwars.services.FilmService;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
public class FilmControllerTest {
	
	private FilmService service;
	
	private FilmRepository repository;
	
	private FilmFilter filmFilter;
	
	private FilmResponse response;
	
	private FilmRequest request;
	
	@Mock
	Page<Film> filmPage;
	
	private Film film;
	
	private Long id = 1L;
	
	private String title;
	
	private Integer episode_id;
	
	private String opening_crawl;
	
	private String director;
	
	private String producer;
	
	private String url;
	
	private LocalDate releaseDate;
	
	@Mock
	private Pageable paginacao = PageRequest.of(0, 10);
	
	@BeforeEach
	void setup() {
		MockitoAnnotations.openMocks(this);
		service = Mockito.mock(FilmService.class);
		repository = Mockito.mock(FilmRepository.class);
		filmFilter = Mockito.mock(FilmFilter.class);
		response = Mockito.mock(FilmResponse.class);
		request = Mockito.mock(FilmRequest.class);
		film = Mockito.mock(Film.class);
		
		this.title = "Allan";
		this.episode_id = 1;
		this.releaseDate = LocalDate.of(2021, 06, 10);
		this.opening_crawl = "teste opening_crawl Swapi";
		this.director = "Steven Spilberg";
		this.producer = "George Lucas";
		this.url = "https://swapi.dev/api/films/1";
		
		Mockito.lenient().when(filmFilter.getTitle()).thenReturn(this.title);
		Mockito.lenient().when(filmFilter.getEpisode_id()).thenReturn(this.episode_id);
		Mockito.lenient().when(filmFilter.getDirector()).thenReturn(this.director);
		Mockito.lenient().when(filmFilter.getProducer()).thenReturn(this.producer);
		
		filmFilter = FilmFilter.builder()
				.title(title)
				.episode_id(episode_id)
				.director(director)
				.producer(producer)
				.build();
		
		request = FilmRequest.builder()
				.id(id)
				.title(title)
				.episode_id(episode_id)
				.release_date(releaseDate)
				.opening_crawl(opening_crawl)
				.director(director)
				.producer(producer)
				.url(url)
				.build();
		
		response = new FilmResponse(
				film.getTitle(),
				film.getEpisode_id(),
				film.getOpening_crawl(),
				film.getDirector(),
				film.getProducer(),
				film.getRelease_date(),
				null,
				null,
				film.getUrl()
				);
	}
	

	@Test
	@DisplayName("Lista todos os filmes")
	void getAllFilmsNotThrowExceptions() {
		
		when(service.findAll(filmFilter, paginacao)).thenReturn(filmPage);
		
		var  responseEntity = ResponseEntity.ok().body(FilmResponse.toFilm(filmPage));
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());		
	}
	
	@Test
	@DisplayName("Consultar todos os filmes com filtro")
    void shouldReturnAllFilms() {
        List<Film> films = new ArrayList<>();
        films.add(new Film());
        
        filmPage = new PageImpl<>(films);
        
        this.title = "A New Hope";
		this.episode_id = null;
		
		filmFilter = FilmFilter.builder()
				.title(title)
				.episode_id(episode_id)
				.build();
        
        given(repository.findAll()).willReturn(films);
        
        Page<Film> expected = repository.findAll(FilmSpecification.buildSpecification(filmFilter), paginacao);
        
        if(Objects.isNull(expected)) {
        	expected = new PageImpl<>(films);
        }
        
        assertEquals(expected, filmPage);
        
    }

	@Test
	@DisplayName("Consultar um filme com sucesso")
	void findByIdFilmSucessfully() {
		
		var responseEntity = ResponseEntity.ok().body(when(service.findById(id)).thenReturn(film));
		
		assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
	}

	@Test
	@DisplayName("Cadastra um filme com sucesso")
	void postFilmWithSucessfully() {
		when(service.create(request)).thenReturn(film);
		
		var responseEntity = ResponseEntity.status(HttpStatus.CREATED).header("id", film.getId().toString()).build();
		
		assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
	}

	@Test
	@DisplayName("Atualiza um filme com sucesso")
	void putFilmWithSucessfully() {
		
		doNothing().when(service).update(id, request);
		
		var responseEntity = ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		
		assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
	}

	@Test
	@DisplayName("Excluir um filme com sucesso")
	void deleteFilmSucessfully() {
		doNothing().when(service).delete(id);
		
		var responseEntity = ResponseEntity.status(HttpStatus.NO_CONTENT).build();
		
		assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
	}
	
	
}
