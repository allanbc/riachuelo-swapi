package br.com.riachuelo.api.starwars.handler.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ErrorDetailDto implements Serializable {
	private static final long serialVersionUID = 1L;

	private String title;
	private Long status;
	private Long timestamp;
	private String mensageDevlopment;


	
}
