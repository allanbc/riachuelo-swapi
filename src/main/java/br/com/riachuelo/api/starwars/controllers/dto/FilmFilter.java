package br.com.riachuelo.api.starwars.controllers.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class FilmFilter {
	
	private String title;
	
	private Integer episode_id;
	
	private String director;
	
	private String producer;
	
}
