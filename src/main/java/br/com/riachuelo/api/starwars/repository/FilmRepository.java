package br.com.riachuelo.api.starwars.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import br.com.riachuelo.api.starwars.models.Film;

@Repository
public interface FilmRepository extends CrudRepository<Film, Long>, JpaSpecificationExecutor<Film>  {

	Optional<Film> findByTitleIgnoreCase(String title);
}
 