package br.com.riachuelo.api.starwars.controllers.dto;

import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.data.domain.Page;

import br.com.riachuelo.api.starwars.models.DateFilm;
import br.com.riachuelo.api.starwars.models.Film;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class FilmResponse extends DateFilm {
	
	private String title;
	
	private Integer episode_id;
	
	private String opening_crawl;
	
	private String director;
	
	private String producer;
	
	private LocalDate replace_date;
	
	private ZonedDateTime createdDate;
	
	private ZonedDateTime updatedDate;
	
	private String url;
	
	public FilmResponse() {
	}

	public FilmResponse(Film film) {
		this.title = film.getTitle();
		this.episode_id = film.getEpisode_id();
		this.opening_crawl = film.getOpening_crawl();
		this.director = film.getDirector();
		this.producer = film.getProducer();
		this.replace_date = film.getRelease_date();
		this.createdDate = film.getCreatedDate();
		this.updatedDate = film.getUpdatedDate();
		this.url = film.getUrl();
	}
	
	public static List<FilmResponse> toFilm(List<Film> films) {
		return films.stream().map(FilmResponse::new).collect(Collectors.toList());
	}

	public static Page<FilmResponse> toFilm(Page<Film> films) {
		return films.map(FilmResponse::new);
	}

}
