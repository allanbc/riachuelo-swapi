package br.com.riachuelo.api.starwars.config.validation;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class FormErrorDto {
	
	private String field;
	private String error;
	private Long status;
	private Long timestamp;
	private String mensagemDesenvolvedor;
}
