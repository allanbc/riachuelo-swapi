package br.com.riachuelo.api.starwars.repository.specification;

import java.util.Objects;

import org.springframework.data.jpa.domain.Specification;

import br.com.riachuelo.api.starwars.controllers.dto.FilmFilter;
import br.com.riachuelo.api.starwars.models.Film;

public class FilmSpecification {
	
	private FilmSpecification() {
	}
	
	public static Specification<Film> likeTitle(String title) {
       return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(root.get("title"), title);
    }

    public static Specification<Film> equalEpisode(Integer episodeId) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.equal(root.get("episode_id"), episodeId);
    }

    public static Specification<Film> likeDirector(String director) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(root.get("director"), director);
    }
    
    public static Specification<Film> likeProducer(String producer) {
        return (root, criteriaQuery, criteriaBuilder) ->
                criteriaBuilder.like(root.get("producer"), producer);
    }
    
    public static Specification<Film> noCondition() {
        return null;
    }
    
	public static Specification<Film> buildSpecification(FilmFilter filter) {
		
		Specification<Film> specification = Objects.nonNull(filter.getTitle()) ?
				Specification.where(FilmSpecification.likeTitle("%" + filter.getTitle() + "%")) :
					Specification.where(FilmSpecification.noCondition());

        if (Objects.nonNull(filter.getEpisode_id())) {
            specification = specification.and(FilmSpecification.equalEpisode(filter.getEpisode_id()));
        }

        if (Objects.nonNull(filter.getDirector())) {
            specification = specification.and(FilmSpecification.likeDirector("%" + filter.getDirector().toUpperCase() + "%"));
        }
        
        if (Objects.nonNull(filter.getProducer())) {
            specification = specification.and(FilmSpecification.likeProducer("%" + filter.getProducer().toUpperCase() + "%"));
        }
        
		return specification;
	}
}
