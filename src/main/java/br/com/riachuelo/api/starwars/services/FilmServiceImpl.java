package br.com.riachuelo.api.starwars.services;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.BeanUtils;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.riachuelo.api.starwars.controllers.dto.FilmFilter;
import br.com.riachuelo.api.starwars.controllers.dto.FilmRequest;
import br.com.riachuelo.api.starwars.models.Film;
import br.com.riachuelo.api.starwars.repository.FilmRepository;
import br.com.riachuelo.api.starwars.repository.specification.FilmSpecification;
import br.com.riachuelo.api.starwars.services.exception.NotFoundException;

@Service
public class FilmServiceImpl implements FilmService {
	
	private FilmRepository repository;
	
	public FilmServiceImpl(FilmRepository repository) {
		this.repository = repository;
	}

	@Override
	public Film create(FilmRequest filmRequest) {
		
		return repository.save(filmRequest.requestFilmBuilder());
	}

	@Override
	public Film findById(Long id) {
		return  repository.findById(id).orElseThrow(
				() -> new NotFoundException("Film not found"));
	}

	@Override
	@Transactional
	@CacheEvict(cacheNames = "registerFilms", allEntries = true)
	public void update(Long id, @Valid FilmRequest requestPut) {
		var filmExists = findById(id);
		
		if(!requestPut.getTitle().equals(filmExists.getTitle())) {
			requestPut.setId(null);
			requestPut.setEpisode_id(requestPut.getEpisode_id()+1);
			create(requestPut);
		}else {
			BeanUtils.copyProperties(requestPut, filmExists, "id");
		    repository.save(filmExists);
		}
	}

	@Override
	public void delete(Long id) {
		Optional<Film> optional = repository.findById(id);
		if (optional.isPresent()) {
			repository.deleteById(id);
		}
	}

	@Override
	public Page<Film> findAll(FilmFilter filmFilter, Pageable paginacao) {
		
		Page<Film> film = this.repository.findAll(FilmSpecification.buildSpecification(filmFilter), paginacao);
		
		if(film.isEmpty()) {
			throw new NotFoundException("Film not found!");
		}
		
		return film;
	}

	
}
