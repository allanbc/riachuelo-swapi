package br.com.riachuelo.api.starwars.controllers;

import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import br.com.riachuelo.api.starwars.client.SWAPIRestTeamplate;
import br.com.riachuelo.api.starwars.controllers.dto.FilmFilter;
import br.com.riachuelo.api.starwars.controllers.dto.FilmRequest;
import br.com.riachuelo.api.starwars.controllers.dto.FilmResponse;
import br.com.riachuelo.api.starwars.models.Film;
import br.com.riachuelo.api.starwars.services.FilmService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;

@RestController
@RequestMapping("/films")
@Tag(name="Find fils", description = "Endpoint to find films")
public class FilmController {

	private FilmService service;
	
	public FilmController(FilmService service, SWAPIRestTeamplate swapi) {
		this.service = service;
	}
	
	@Operation(
			summary = "Cadastro",
			description = "Cadastro",
			tags = {"Register Films"}
			)
	@PostMapping
	public ResponseEntity<Film> create(@RequestBody FilmRequest filmRequest){
		
		var film = service.create(filmRequest);
        
        return ResponseEntity.status(HttpStatus.CREATED).header("id", film.getId().toString()).build();
		
	}
	
	@Operation(
			summary = "Update",
			description = "Update",
			tags = {"Update Films"}
			)
	@PutMapping("/{id}")
	@Transactional
	public ResponseEntity<Void> update(@PathVariable Long id, @RequestBody @Valid FilmRequest requestPut) {
		
		service.update(id, requestPut);
		
		return ResponseEntity.noContent().build();
	}
	
	@Operation(
			summary = "Consulta",
			description = "Consulta",
			tags = {"Find by ID Films"}
			)
	@GetMapping("/{id}")
	public ResponseEntity<FilmResponse> findById(@PathVariable("id") Long id){
		
		var response = new FilmResponse(service.findById(id));
		
		return ResponseEntity.status(HttpStatus.OK).body(response);
	}
	
	
	@Operation(
			summary = "Consulta",
			description = "Consulta",
			parameters = {
					@Parameter(in = ParameterIn.HEADER, name = "title", description = "Film title", required =true),
					@Parameter(in = ParameterIn.HEADER, name = "epsiode_id", description = "Film episode", required =true),
			},
			tags = {"Find All Films"}
			)
	@ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Requisição com sucesso", content = @Content(mediaType = "application/json", schema = @Schema(implementation = FilmResponse.class))),
            @ApiResponse(responseCode = "400", content = @Content(mediaType = "application/json", schema = @Schema(title = "Resource not found"))),
            @ApiResponse(responseCode = "403", content = @Content(mediaType = "application/json", schema = @Schema(title = "Missing or invalid request body"))),
            @ApiResponse(responseCode = "500", content = @Content(mediaType = "application/json", schema = @Schema(title = "Internal Error"))),
    })
	@GetMapping
	public ResponseEntity<Page<FilmResponse>> findAll(
			 @RequestParam(value = "title", required = false) String title,
			 @RequestParam(value = "episode_id", required = false) Integer episode_id,
			 @RequestParam(value = "director", required = false) String director,
			 @RequestParam(value = "producer", required = false) String producer,
			 @PageableDefault(page = 0, size = 10) Pageable paginacao
			){
		
		var filmFilter = FilmFilter.builder()
				.title(title)
				.episode_id(episode_id)
				.director(director)
				.producer(producer)
				.build();
		
		Page<Film> film = service.findAll(filmFilter, paginacao);
		
		return ResponseEntity.ok()
				.body(FilmResponse.toFilm(film));
	}
	
	@Operation(
			summary = "Delete",
			description = "Delete",
			tags = {"Delete Films"}
			)
	@DeleteMapping("/{id}")
	public ResponseEntity<Void> delete(@PathVariable Long id){
		service.delete(id);
		return ResponseEntity.noContent().build();
	}
}
