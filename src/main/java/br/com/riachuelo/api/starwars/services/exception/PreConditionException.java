package br.com.riachuelo.api.starwars.services.exception;

public class PreConditionException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PreConditionException(String message, Throwable cause) {
		super(message, cause);
	}

	public PreConditionException(String message) {
		super(message);
	}
	
	
}
