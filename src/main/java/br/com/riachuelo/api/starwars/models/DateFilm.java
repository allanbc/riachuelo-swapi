package br.com.riachuelo.api.starwars.models;

import java.time.ZonedDateTime;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.Data;

@Data
@MappedSuperclass
public abstract class DateFilm {

  @UpdateTimestamp
  @Column(name = "dt_updated", insertable = false)
  private ZonedDateTime updatedDate;

  @CreationTimestamp
  @Column(name = "dt_created", updatable = false, columnDefinition = "DATETIME(3) DEFAULT NOW(3)")
  private ZonedDateTime createdDate;
}