package br.com.riachuelo.api.starwars.handler;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;

import br.com.riachuelo.api.starwars.handler.dto.ErrorDetailDto;
import br.com.riachuelo.api.starwars.services.exception.NotFoundException;

@ControllerAdvice
@RestController
public class ResourceExceptionHandler {
	
	@ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorDetailDto> handleAllExceptions(Exception ex, HttpServletRequest request) {
        var error = new ErrorDetailDto();
        error.setStatus(400L);
        error.setTitle("An error ocurred in the application");
        error.setMensageDevlopment(ExceptionUtils.getRootCauseMessage(ex));
        error.setTimestamp(System.currentTimeMillis());
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
    }
	
	 @ExceptionHandler(NotFoundException.class)
	 public ResponseEntity<ErrorDetailDto> handleRecursoNaoEncontradoException(NotFoundException e, HttpServletRequest request) {

	 	var error = new ErrorDetailDto();
        error.setStatus(404L);
        error.setTitle("The feature is not available.");
        error.setMensageDevlopment(ExceptionUtils.getRootCauseMessage(e));
        error.setTimestamp(System.currentTimeMillis());

        return ResponseEntity.status(HttpStatus.NOT_FOUND).body(error);
	  }
}
