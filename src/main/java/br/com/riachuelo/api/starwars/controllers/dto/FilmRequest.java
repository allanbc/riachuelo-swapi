package br.com.riachuelo.api.starwars.controllers.dto;

import java.time.LocalDate;

import javax.validation.constraints.NotBlank;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

import br.com.riachuelo.api.starwars.models.Film;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({ "id", "nome", "clima", "terreno" })
public class FilmRequest {
	
	private Long id;
	
	@NotBlank
	@JsonProperty(required = true)
	private String title;
	
	private Integer episode_id;
	
	private String opening_crawl;
	
	private String director;
	
	private String producer;
	
	private LocalDate release_date;
	
	private String url;
	
	public Film requestFilmBuilder() {
		return Film.builder()
				.title(this.title)
				.episode_id(episode_id)
				.opening_crawl(opening_crawl)
				.director(director)
				.producer(producer)
				.release_date(release_date)
				.url(url)
				.build();
	}


}
