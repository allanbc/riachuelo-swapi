package br.com.riachuelo.api.starwars.services;

import javax.validation.Valid;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import br.com.riachuelo.api.starwars.controllers.dto.FilmFilter;
import br.com.riachuelo.api.starwars.controllers.dto.FilmRequest;
import br.com.riachuelo.api.starwars.models.Film;

public interface FilmService {
	
	Film create(FilmRequest filmRequest);
	
	Film findById(Long id);
	
	Page<Film> findAll(FilmFilter filmFilter, Pageable paginacao);
	
	void update(Long id, @Valid FilmRequest requestPut);
	
	void delete(Long id);

}
